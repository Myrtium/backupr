::------------------------------------------------------------------------------------------------::
:: Execute unit tests                                                                           ::
::------------------------------------------------------------------------------------------------::
:: Suppress echo of commands
@ECHO OFF

:: Request version info from python executable. If error occurs during request ...
python -V 2>nul || (
    :: Print error message to terminal
    echo Error: 'python.exe' could not be found.
    echo Most likely cause: no Python distribution is installed on your sytem.
    echo.

    :: Exit terminal after key press
    echo Press any key to close this window ...
    pause >nul
    exit
)

:: Add source root directory to python search paths
if defined PYTHONPATH (
    set PYTHONPATH=%~dp0src;%PYTHONPATH%
) else (
    set PYTHONPATH=%~dp0src
)

python -m CLI %*

echo.
echo Press any key to close this window ...
pause > nul


