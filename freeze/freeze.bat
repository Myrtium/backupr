:: Suppress echo of commands
@ECHO OFF

:: Set this file's directory as current working directory
pushd %~dp0

call ../venv/Scripts/activate.bat

:: Run pyinstaller
pyinstaller "%~dp0freeze.spec" -y


:: Restore previous working directory
popd

:: If no 'background mode' flag was given ...
if not "%~1"=="-b" (
    :: Keep terminal open until user feedback
    echo.
    echo Press any key to close this window ...
    pause > nul
)
