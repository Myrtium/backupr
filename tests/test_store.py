"""
Filename: ${FILE_NAME}
Created on 27.05.2020
Creator: Myrtium
"""
# External modules
import logging
from unittest import TestCase
from pathlib import Path
from store import Store

# Project related modules

logger = logging.getLogger().getChild(__name__)
test_directory = Path(__file__).parent / Path("testfiles/example_dir")


class TestStore(TestCase):

    # Store-constructor
    def test_store_empty_arguments(self):
        files_result = {}
        meta_result = {}

        store = Store()
        self.assertDictEqual(store.files, files_result, "Files-dict not empty")
        self.assertDictEqual(store.meta, meta_result, "Meta-dict not empty")

    def test_store_filled_arguments(self):
        files = {"file1": 1234, "file2": 12345}
        meta = {"arg1": 1, "arg2": 2}

        expected_files = {Path("file1"): 1234, Path("file2"): 12345}
        expected_meta = {"arg1": 1, "arg2": 2}

        store = Store(files=files, meta=meta)
        self.assertDictEqual(store.files, expected_files, "Files-dict did not match the expected result")
        self.assertDictEqual(store.meta, expected_meta, "Meta-dict did not match the expected result")

    # compare()-function
    def test_compare_files_equal_only(self):
        store1 = Store(files={"file1": 1234, "file2": 12345}, meta={"eq": 2})
        store2 = Store(files={"file1": 1234, "file2": 12345}, meta={"not_eq": 2})

        self.assertFalse(store1.equal_to(store2), "Both stores are equal")

    def test_compare_meta_equal_only(self):
        store1 = Store(files={"file1": 1234}, meta={"eq": 2, "another": "one"})
        store2 = Store(files={"file1": 1234, "file2": 12345}, meta={"eq": 2, "another": "one"})

        self.assertFalse(store1.equal_to(store2), "Both stores are equal")

    def test_compare_both_equal(self):
        store1 = Store(files={"file1": 1234, "file2": 12345}, meta={"eq": 2, "another": "one"})
        store2 = Store(files={"file1": 1234, "file2": 12345}, meta={"eq": 2, "another": "one"})

        self.assertTrue(store1.equal_to(store2), "Both stores are not equal")

    def test_compare_input_as_path(self):
        store1 = Store(files={Path("file1"): 1234, Path("file2"): 12345}, meta={"eq": 2, "another": "one"})
        store2 = Store(files={"file1": 1234, "file2": 12345}, meta={"eq": 2, "another": "one"})

        self.assertTrue(store1.equal_to(store2), "Both stores are not equal")

    def test_compare_both_none(self):
        store1 = Store()
        store2 = Store()

        self.assertTrue(store1.equal_to(store2), "Both stores are not equal")

    # from_dict()-function
    def test_from_dict_empty(self):
        files_result = {}
        meta_result = {}
        store = Store.from_dict({})

        self.assertDictEqual(store.files, files_result, "Files-dict assertion")
        self.assertDictEqual(store.meta, meta_result, "Meta-dict assertion")

    def test_from_dict_empty_dicts(self):
        input_dict = {"files": None, "meta": None}
        files_result = {}
        meta_result = {}
        store = Store.from_dict(input_dict)

        self.assertDictEqual(store.files, files_result, "Files-dict assertion")
        self.assertDictEqual(store.meta, meta_result, "Meta-dict assertion")

    def test_from_dict_filled(self):
        files = {"file1": 1234, "file2": 12345}
        meta = {"eq": 2, "another": "one"}

        input_dict = {
            "files": files,
            "meta": meta,
        }
        expected_files = {Path("file1"): 1234, Path("file2"): 12345}
        expected_meta = {"eq": 2, "another": "one"}
        store = Store.from_dict(input_dict)

        self.assertDictEqual(store.files, expected_files, "Files-dict assertion")
        self.assertDictEqual(store.meta, expected_meta, "Meta-dict assertion")

    # to_dict()-function
    def test_to_dict_empty_store(self):
        store = Store().to_dict()

        expected = {"files": {}, "meta": {}}
        self.assertDictEqual(store, expected)

    def test_to_dict_filled_store(self):
        store = Store(files={"file1": 1234, "file2": 12345}, meta={"eq": 2, "another": "one"})

        expected = {
            "files": {
                Path("file1"): 1234,
                Path("file2"): 12345
            },
            "meta": {
                "eq": 2,
                "another": "one"
            }
        }

        self.assertDictEqual(store.to_dict(), expected)

    # index_files()-function
    def test_index_files_dir_does_not_exist(self):
        with self.assertRaises(NotADirectoryError):
            Store.index_files(Path("fadfasf"), recursive=True, ignore_hidden=False)

    def test_index_files_recursive(self):
        store = Store.index_files(test_directory, recursive=True, ignore_hidden=False)

        expected_files = [
            Path('.hidden1'),
            Path('dir1'),
            Path('file1.txt'),
            Path('file2.txt'),
            Path('dir1/.dir1.2'),
            Path("dir1/.hidden1.1"),
            Path('dir1/dir1.1'),
            Path('dir1/file1.1.txt'),
            Path('dir1/file1.2.txt'),
            Path('dir1/dir1.1/file1.1.1.txt'),
            Path('dir1/dir1.1/file1.1.2.txt'),
            Path('dir1/.dir1.2/file1.2.1.txt')
        ]

        expected_meta = {
                "root": test_directory,
                "directories": 3,
                "files": 9
        }

        self.assertListEqual(sorted(store.files.keys()), sorted(expected_files))
        self.assertDictEqual(store.meta, expected_meta)

    def test_index_files_not_recursive(self):
        store = Store.index_files(test_directory, recursive=False, ignore_hidden=False)

        expected_files = [
            Path('.hidden1'),
            Path('dir1'),
            Path('file1.txt'),
            Path('file2.txt'),
        ]

        expected_meta = {
                "root": test_directory,
                "directories": 1,
                "files": 3
        }

        self.assertListEqual(sorted(store.files.keys()), sorted(expected_files))
        self.assertDictEqual(store.meta, expected_meta)

    def test_index_files_recursive_ignore_hidden(self):
        store = Store.index_files(test_directory, recursive=True, ignore_hidden=True)

        expected_files = [
            Path('dir1'),
            Path('file1.txt'),
            Path('file2.txt'),
            Path('dir1/dir1.1'),
            Path('dir1/file1.1.txt'),
            Path('dir1/file1.2.txt'),
            Path('dir1/dir1.1/file1.1.1.txt'),
            Path('dir1/dir1.1/file1.1.2.txt'),
        ]

        expected_meta = {
                "root": test_directory,
                "directories": 2,
                "files": 6
        }

        self.assertListEqual(sorted(store.files.keys()), sorted(expected_files))
        self.assertDictEqual(store.meta, expected_meta)

    def test_index_files_ignore_file(self):
        to_ignore = [
            Path("file1.txt"),
            Path('dir1/dir1.1'),
        ]
        store = Store.index_files(test_directory, recursive=True, ignore_hidden=False, ignore_file=to_ignore)

        expected_files = [
            Path('.hidden1'),
            Path('dir1'),
            Path('file2.txt'),
            Path('dir1/.dir1.2'),
            Path("dir1/.hidden1.1"),
            Path('dir1/file1.1.txt'),
            Path('dir1/file1.2.txt'),
            Path('dir1/.dir1.2/file1.2.1.txt')
        ]

        expected_meta = {
                "root": test_directory,
                "directories": 2,
                "files": 6
        }

        self.assertListEqual(sorted(store.files.keys()), sorted(expected_files))
        self.assertDictEqual(store.meta, expected_meta)

    def test_index_files_ignore_file_invalid(self):
        to_ignore = [
            Path("file1.txt"),
            Path('dir1/dir1.1'),
            Path("INVALID"),
        ]
        store = Store.index_files(test_directory, recursive=True, ignore_hidden=False, ignore_file=to_ignore)

        expected_files = [
            Path('.hidden1'),
            Path('dir1'),
            Path('file2.txt'),
            Path('dir1/.dir1.2'),
            Path("dir1/.hidden1.1"),
            Path('dir1/file1.1.txt'),
            Path('dir1/file1.2.txt'),
            Path('dir1/.dir1.2/file1.2.1.txt')
        ]

        expected_meta = {
                "root": test_directory,
                "directories": 2,
                "files": 6
        }

        self.assertListEqual(sorted(store.files.keys()), sorted(expected_files))
        self.assertDictEqual(store.meta, expected_meta)