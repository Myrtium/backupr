"""
Filename: ${FILE_NAME}
Created on 27.05.2020
Creator: Myrtium
"""
# External modules
import logging
from unittest import TestCase
from pathlib import Path

# Project related modules
from backup import Backup

logger = logging.getLogger().getChild(__name__)


class TestBackup(TestCase):

    def test_backup_constructor_correct(self):
        source = Path("source/path")
        destination = Path("dest/path")
        force = False
        recursive = True
        ignore_hidden = False
        compression = "deflated"
        ignore_file = []
        rotations = 5

        back = Backup(
            source=source,
            destination=destination,
            force=force,
            recursive=recursive,
            ignore_hidden=ignore_hidden,
            compression=compression,
            ignore_file=ignore_file,
            rotations=rotations,
        )

        self.assertEqual(back.source, source)
        self.assertEqual(back.destination, destination)
        self.assertEqual(back.force, force)
        self.assertEqual(back.recursive, recursive)
        self.assertEqual(back.ignore_hidden, ignore_hidden)
        self.assertEqual(back.compression, compression)
        self.assertEqual(back.ignore_file, ignore_file)
        self.assertEqual(back.rotations, rotations)

    def test_backup_constructor_paths_as_string(self):
        source = "source/path"
        destination = "dest/path"
        force = False
        recursive = True
        ignore_hidden = False
        compression = "deflated"
        ignore_file = []
        rotations = 5

        back = Backup(
            source=source,
            destination=destination,
            force=force,
            recursive=recursive,
            ignore_hidden=ignore_hidden,
            compression=compression,
            ignore_file=ignore_file,
            rotations=rotations,
        )

        self.assertEqual(back.source, Path(source))
        self.assertEqual(back.destination, Path(destination))
        self.assertEqual(back.force, force)
        self.assertEqual(back.recursive, recursive)
        self.assertEqual(back.ignore_hidden, ignore_hidden)
        self.assertEqual(back.compression, compression)
        self.assertEqual(back.ignore_file, ignore_file)
        self.assertEqual(back.rotations, rotations)

    def test_backup_constructor_arguments_none(self):
        source = "source/path"
        destination = "dest/path"
        force = None
        recursive = None
        ignore_hidden = None
        compression = None
        ignore_file = None
        rotations = None

        back = Backup(
            source=source,
            destination=destination,
            force=force,
            recursive=recursive,
            ignore_hidden=ignore_hidden,
            compression=compression,
            ignore_file=ignore_file,
            rotations=rotations,
        )

        self.assertEqual(back.source, Path(source))
        self.assertEqual(back.destination, Path(destination))
        self.assertEqual(back.force, False)
        self.assertEqual(back.recursive, True)
        self.assertEqual(back.ignore_hidden, False)
        self.assertEqual(back.compression, "deflated")
        self.assertEqual(back.ignore_file, [])
        self.assertEqual(back.rotations, 0)

    def test_to_dict(self):
        source = Path("source/path")
        destination = Path("dest/path")
        force = False
        recursive = True
        ignore_hidden = False
        compression = "deflated"
        ignore_file = []
        rotations = 5

        back = Backup(
            source=source,
            destination=destination,
            force=force,
            recursive=recursive,
            ignore_hidden=ignore_hidden,
            compression=compression,
            ignore_file=ignore_file,
            rotations=rotations
        )

        expected = {
            "source": source,
            "destination":destination,
            "force": force,
            "recursive": recursive,
            "ignore_hidden": ignore_hidden,
            "compression": compression,
            "ignore_file": ignore_file,
            "rotations": rotations,
        }

        self.assertDictEqual(back.to_dict(), expected)

    def test_from_dict(self):
        source = Path("source/path")
        destination = Path("dest/path")
        force = False
        recursive = True
        ignore_hidden = False
        compression = "deflated"
        ignore_file = []
        rotations = 5

        input_dict = {
            "source": source,
            "destination": destination,
            "force": force,
            "recursive": recursive,
            "ignore_hidden": ignore_hidden,
            "compression": compression,
            "ignore_file": ignore_file,
            "rotations": rotations,
        }

        back = Backup.from_dict(input_dict)
        self.assertDictEqual(back.to_dict(), input_dict)