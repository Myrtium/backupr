"""
Filename: ${FILE_NAME}
Created on 29.05.2020
Creator: Myrtium
"""
# External modules
import logging
from unittest import TestCase
from pathlib import Path

# Project related modules
from config import Config
from backup import Backup

logger = logging.getLogger().getChild(__name__)


class TestConfig(TestCase):
    def test_constructor_correct(self):
        logfile = Path("logfile/path")
        loglevel = "INFO"
        wait = "30s"

        back = Backup(
            source=Path("source/path"),
            destination=Path("dest/path"),
            force=False,
            recursive=True,
            ignore_hidden=False,
            compression="deflated",
            ignore_file=[],
            rotations=5,
        )

        back2 = Backup(
            source=Path("source/path2"),
            destination=Path("dest/path2"),
            force=True,
            recursive=False,
            ignore_hidden=True,
            compression="lzma",
            ignore_file=[],
            rotations=2,
        )

        backup_dict = {"back": back, "back2": back2}

        conf = Config(logfile=logfile, loglevel=loglevel, wait=wait, backup_dict=backup_dict)

        self.assertEqual(conf.logfile, logfile)
        self.assertEqual(conf.loglevel, loglevel)
        self.assertEqual(conf.wait, wait)
        self.assertDictEqual(conf.backup_dict, backup_dict)

    def test_constructor_incorrect(self):
        logfile = Path("logfile/path")
        loglevel = "INFO"
        wait = "30s"

        backup_dict = {}

        conf = Config(logfile=str(logfile),loglevel=None, wait=None)

        self.assertEqual(conf.logfile, logfile)
        self.assertEqual(conf.loglevel, loglevel)
        self.assertEqual(conf.wait, wait)
        self.assertDictEqual(conf.backup_dict, backup_dict)

    def test_to_dict_correct(self):
        logfile = Path("logfile/path")
        loglevel = "INFO"
        wait = "30s"

        back = Backup(
            source=Path("source/path"),
            destination=Path("dest/path"),
            force=False,
            recursive=True,
            ignore_hidden=False,
            compression="deflated",
            ignore_file=[],
            rotations=5,
        )

        back2 = Backup(
            source=Path("source/path2"),
            destination=Path("dest/path2"),
            force=True,
            recursive=False,
            ignore_hidden=True,
            compression="lzma",
            ignore_file=[],
            rotations=2,
        )

        expected_dict = {
            "settings": {
                "logfile": logfile,
                "loglevel": loglevel,
                "wait": wait,
            },
            "config": {"back": back.to_dict(), "back2": back2.to_dict()}
        }

        conf = Config(logfile=logfile, loglevel=loglevel, wait=wait, backup_dict={"back": back, "back2": back2})

        self.assertDictEqual(conf.to_dict(), expected_dict)
