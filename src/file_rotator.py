"""
Filename: file_rotator.py
Created on 29.06.2020
Creator: Myrtium
"""
# External modules
import logging
import re
from pathlib import Path

# Project related modules

logger = logging.getLogger().getChild(__name__)


class FileRotator:
    def __init__(self, directory_path: [str, Path], filename: str, max_files: int, pattern: str, padding_char: str = "0", padding_len: int = None):
        self.directory_path = Path(directory_path)
        self.filename = filename
        self.max_files = max_files
        self.pattern = pattern
        self.padding_char = padding_char
        self.padding_leng = padding_len or len(str(max_files))

    def get_existing_rotations(self):
        pattern = "^" + re.escape(self.filename) + self.pattern + "$"

        rotations = []

        for current in self.directory_path.iterdir():
            if current.is_file() and re.fullmatch(pattern, current.name):
                rotations.append(current)
        return rotations

    def rotate(self, suppress_cleanup: bool = False):
        rotations = self.get_existing_rotations()

        for num in reversed(range(len(rotations))):
            file = rotations[num]
            if not suppress_cleanup and len(rotations) > self.max_files:
                # Exit statement as soon as only 1 file remains
                if len(rotations) <= 1:
                    break

                try:
                    file.unlink()
                except FileNotFoundError as e:
                    logger.warning(e)
                rotations.remove(file)
                logger.info(f"Removed file '{file.name}' during rotation")
            else:
                try:
                    number = int(file.suffix[1:])
                    number += 1
                    file.rename(file.parent / (file.stem + "." + str(number).rjust(self.padding_leng, self.padding_char)))
                except ValueError:
                    file.rename(file.parent / (file.name + "." + str(1).rjust(self.padding_leng, self.padding_char)))
                except FileNotFoundError as e:
                    logger.warning(e)
