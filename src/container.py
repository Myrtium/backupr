"""
Filename: BackupContainer.py
Created on 26.05.2020
Creator: Myrtium
"""
from backup import Backup


class BackupContainer:
    def __init__(self):
        self._backups = {}

    def __iter__(self):
        return self._backups.__iter__()

    def add(self, name: str, backup: Backup):
        if name not in self._backups.keys():
            self._backups[name] = backup

    def remove(self, name):
        if name in self._backups.keys():
            del self._backups[name]

    def get(self, name):
        return self._backups.get(name)

    def exist(self, name):
        return name in self._backups.keys()

    def items(self):
        return self._backups.items()


if __name__ == "__main__":
    from pathlib import Path
    bla = BackupContainer()
    back = Backup(Path("wdfs"), Path("sdafs"), None, None, None, None)
    back2 = Backup(Path("wdfs"), Path("sdafs"), None, None, None, None)
    bla.add("Test", back)
    bla.add("Test2", back2)

    for ba, val in bla.items():
        print(f"{ba}: {str(val.source)}")