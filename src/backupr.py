"""
Filename: backupr.py
Created on 02.09.2019
Creator: Myrtium
"""
# External modules
import logging
from pathlib import Path

# Project related modules
import constants as const
from backup import Backup
from store import Store
from util import zip_it, load_json, write_json

logger = logging.getLogger().getChild(__name__)


class Backupr:

    def __init__(self, settings: Backup):
        self.settings = settings
        self.store_path = Path(self.settings.source) / const.BACKUPR_STORE_FILENAME
        self.cache = None

        self.store = self._get_store_file()

    def has_changed(self, cache: bool = True):
        ignore_file = self._get_ignore_file_list()

        logger.info(f"Indexing '{self.settings.source}'")
        new_store = Store.index_files(self.settings.source, self.settings.recursive, self.settings.ignore_hidden, ignore_file)
        logger.info(f"New Store meta: {new_store.meta}")

        logger.debug(f"Cache store: '{cache}'")
        if cache is True:
            self.cache = new_store

        return not new_store.equal_to(self.store)

    def backup_now(self, use_cached: bool = True, cache: bool = True):
        if self.cache and use_cached is True:
            store = self.cache
        else:
            ignore_file = self._get_ignore_file_list()
            logger.info(f"Indexing '{self.settings.source}'")
            store = Store.index_files(self.settings.source, self.settings.recursive, self.settings.ignore_hidden, ignore_file)
            logger.info(f"Store meta: {store.meta}")

            if cache is True:
                self.cache = store

        logger.info(f"Backup started for '{self.settings.source}'")
        zip_it(self.settings.destination, self.settings.source, store.files.keys(), self.settings.compression_int)
        logger.info(f"Backup finished for '{self.settings.source}'")

    def export_store(self, use_cached: bool = True, store: Store = None):
        if use_cached is True and self.cache is not None:
            export = self.cache.to_dict(serializable=True)
            export["meta"]["root"] = str(export["meta"]["root"])
            logger.info(f"Export store (cached) to '{self.store_path}'")
            write_json(export, self.store_path)
            logger.info(f"Export finished'")
        elif use_cached is True and self.cache is None:
            logger.error("Could not export store. 'use_cached' is True but no cache found!")
        elif use_cached is False and store is not None:
            export = store.to_dict(serializable=True)
            export["meta"]["root"] = str(export["meta"]["root"])
            logger.info(f"Export store (not cached) to '{self.store_path}'")
            write_json(export, self.store_path)
            logger.info(f"Export finished'")
        elif use_cached is False and store is None:
            logger.error("Could not export store. 'use_cached' is False and no store was given!")
        else:
            logger.error(f"Could not export store. Input variables wrong! 'use_cached'='{use_cached}' 'store'={store}")

    def _get_ignore_file_list(self, include_store_path: bool = True):
        if include_store_path is True:
            ignore_file = list(self.settings.ignore_file)  # Shallow copy
            ignore_file.append(const.BACKUPR_STORE_FILENAME)
            return ignore_file
        else:
            return self.settings.ignore_file

    def _get_store_file(self):
        if self.store_path.exists():
            logger.info(f"Reload store from '{self.store_path}'.")
            load = load_json(self.store_path)
            store = Store.from_dict(load)
            logger.info(f"Store meta: {store.meta}")
            return store
        else:
            logger.warning(f"Did not find store at {self.store_path}. Warning can be ignored, if this directory was just added to Backupr.")
            return Store()
