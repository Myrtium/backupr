"""
Filename: resource_handler.py
Created on 25.06.2019
Creator: Myrtium
"""
# External modules
import logging
import json
from pathlib import Path

# Own imports and modules


logger = logging.getLogger().getChild(__name__)

__instance = None


def get_resource_handler():
    global __instance
    return __instance


def set_resource_handler(r_handler):
    global __instance
    __instance = r_handler


class ResourceHandler:
    def __init__(self, root_directory, paths_file):
        self.paths_file = Path(root_directory, paths_file)
        self.root_directory = root_directory

        with open(self.paths_file, "r") as read_file:
            self.raw = json.load(read_file)

        self._files = self.raw["files"]

        for key in self._files:
            file = self._files.get(key)
            path = self.get(key)
            exists = path.exists()
            optional = file.get("optional")

            if not exists and optional is not True:
                raise FileNotFoundError(f"File at '{path}' does not exist")
            elif not exists and optional is True:
                logger.warning(f"File at '{path}' does not exist but is marked as an optional file.")
            else:
                if key not in self.__dict__:
                    self.__dict__[key] = file
        logger.debug(f"Resource file paths loaded from '{self.paths_file}'")
        logger.debug(f"    Version:'{self.raw['general']['version']}' environment:'{self.raw['general']['environment']}' platform: '{self.raw['general']['platform']}'")
        logger.debug(f"    {len(self._files)} file paths found")

    def get(self, file_identifier):
        try:
            return Path(self.root_directory, self._files[file_identifier].get("path"))

        except KeyError as v:
            logger.error(v.__traceback__)
            raise FileNotFoundError(f"File with identifier '{file_identifier}' was not found!")
