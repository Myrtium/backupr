"""
Filename: config.py
Created on 24.05.2020
Creator: Myrtium
"""

import constants as const
from exceptions import ConfigError
from dataclasses import dataclass, field
from typing import Dict
from pathlib import Path
from backup import Backup


@dataclass
class Config:
    logfile: Path
    loglevel: str
    wait: str
    backup_dict: Dict[str, Backup] = field(default_factory=dict)

    def __post_init__(self):
        if self.logfile is None:
            self.logfile = const.BACKUPR_DEFAULT_LOGFILE_PATH

        if not isinstance(self.logfile, Path):
            self.logfile = Path(self.logfile)

        if self.loglevel is None:
            self.loglevel = const.BACKUPR_DEFAULT_LOG_LEVEL

        if self.wait is None:
            self.wait = const.BACKUPR_DEFAULT_WAIT

        if self.backup_dict is None:
            self.backup_dict = {}

    @staticmethod
    def from_dict(input: dict) -> "Config":
        settings = input.get("settings")
        if settings is None:
            raise ConfigError("'settings' Key not found.")

        logfile = Path(settings.get("logfile"))
        loglevel = settings.get("loglevel")
        wait = settings.get("wait")
        config = input.get("config")

        backup_dict = {}
        if config is not None and isinstance(config, dict):
            backup_dict = {name: Backup.from_dict(curr) for name, curr in config.items()}

        conf = Config(
            logfile=logfile,
            loglevel=loglevel,
            wait=wait,
            backup_dict=backup_dict
        )

        return conf

    def to_dict(self, serializable=False) -> dict:
        config = {name: curr.to_dict(serializable) for name, curr in self.backup_dict.items()}

        logfile = str(self.logfile) if serializable else self.logfile

        output = {
            "settings": {
                "logfile": logfile,
                "loglevel": self.loglevel,
                "wait": self.wait,
            },
            "config": config,
        }
        return output

    def to_str(self, detailed=False):
        output = f"""
        logfile: {str(self.logfile)},
        loglevel: {self.loglevel},
        wait: {self.wait},
        config:
        """

        for name, back in self.backup_dict.items():
            output += f"  name: {name}\n"
            output += back.to_str(detailed=detailed) + "\n"

        return output
