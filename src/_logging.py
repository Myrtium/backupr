"""
Filename: _logging.py
Created on 24.09.2019
Creator: Myrtium
"""
# External modules
import logging
import sys
from logging import FileHandler

# Project related modules

logger = logging.getLogger()


def exception_logger(type, value, tb):
    logger.critical("Critical Error occured:", exc_info=(type, value, tb))


def init_logging(log_path, loglevel):
    """
    Set up the general logging module to also capture warnings.

    Set up the basic appearance of a log entry as well as the default logging level.
    """
    levels = {
        'CRITICAL': logging.CRITICAL,
        'ERROR': logging.ERROR,
        'WARNING': logging.WARNING,
        'INFO': logging.INFO,
        'DEBUG': logging.DEBUG
        }

    loglevel = levels["INFO"] if loglevel.upper() not in levels.keys() else levels[loglevel.upper()]

    logging.captureWarnings(True)
    logger.setLevel(loglevel)
    logging.basicConfig(level=loglevel, format="%(asctime)s - %(levelname)-8s - %(message)s")
    log_formatter = logging.Formatter("%(asctime)s - %(levelname)-8s - %(message)s")
    sys.excepthook = exception_logger

    try:
        log_file_handler = FileHandler(log_path, mode="a", encoding="utf-8")
        log_file_handler.setLevel(loglevel)
        log_file_handler.setFormatter(log_formatter)

        logger.addHandler(log_file_handler)

    except PermissionError as e:
        logger.warning(e)
        logger.warning("No log file created (is there another instance running?).")