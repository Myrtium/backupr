"""
Filename: service.py
Created on 19.09.2019
Creator: Myrtium
"""
# External modules
import datetime
import logging

# Project related modules
import constants as const
from config import Config
from backupr import Backupr
from file_rotator import FileRotator
logger = logging.getLogger()


class Service:

    def __init__(self, config: Config):
        self.config = config

    def run(self):
        backups = self.config.backup_dict

        timestamp = datetime.datetime.now().strftime("%Y-%b-%d_%H-%M-%S")
        logger.info(f"*************** Start Backup at {timestamp} ***************")

        for name, conf in backups.items():
            logger.info(f"Start processing '{name}' directory '{conf.source}'")

            backup = Backupr(conf)

            if not conf.source.exists():
                logger.error(f"Given directory '{conf.source}' does not exist!")
                continue

            if not conf.source.is_dir():
                logger.error(f"Given path '{conf.source}' is not a directory!")
                continue

            has_changed = backup.has_changed()

            if has_changed or conf.force:
                logger.info(f"Directory '{conf.source}' has been modified.'")
                logger.debug(f"'force_rewrite'='{conf.force}'")

                logger.info(f"Rotate files")
                fr = FileRotator(conf.destination.parent, conf.destination.name, conf.rotations, const.BACKUPR_ROTATION_PATTERN)
                fr.rotate()
                logger.info(f"Rotation done")

                backup.backup_now()
                backup.export_store()

                logger.info(f"Finished processing '{name}'")
            else:
                logger.info("Nothing has changed.")

        timestamp = datetime.datetime.now().strftime("%Y-%b-%d_%H-%M-%S")
        logger.info(f"*************** Finish Backup at {timestamp} ***************")

