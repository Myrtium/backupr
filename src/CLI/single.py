"""
Filename: single.py
Created on 26.05.2020
Creator: Myrtium
"""
# External modules
import logging

# Project related modules
import constants as const
import CLI.parameter as param
from _logging import init_logging
from util import gen_argument, str2bool
from backupr import Backupr
from backup import Backup

logger = logging.getLogger().getChild(__name__)

PARAMETER = [
    param.SRC_PATH,
    param.DEST_FILE,
    param.FORCE_BACKUP,
    param.LOG,
    param.LOGLEVEL,
    param.RECURSIVE,
    param.IGNORE_HIDDEN,
    param.IGNORE_FILE,
    param.COMPRESSION,
]


def generate_subparser_single(subparser):
    parser = subparser.add_parser("backup", help="Start single backup")

    for arg in PARAMETER:
        gen_argument(parser, arg)
    parser.set_defaults(func=start_backup)


def start_backup(args):
    source = args.src
    destination = args.dest
    recursive = str2bool(args.recursive, True)
    ignore_hidden = str2bool(args.ignore_hidden, False)
    force = str2bool(args.force, False)
    compression = args.compression

    if args.ignore_file is not None:
        ignore_file = set(args.ignore_file)
    else:
        ignore_file = []

    if args.log is not None:
        init_logging(args.log, args.loglevel or const.BACKUPR_DEFAULT_LOG_LEVEL)

    logfile_path = args.log or const.BACKUPR_DEFAULT_LOGFILE_PATH
    loglevel = args.loglevel or const.BACKUPR_DEFAULT_LOG_LEVEL
    init_logging(logfile_path, loglevel)

    config = Backup(
        source=source,
        destination=destination,
        force=force,
        recursive=recursive,
        ignore_hidden=ignore_hidden,
        compression=compression,
        ignore_file=ignore_file,
        rotations=0,
    )

    if not config.source.exists():
        logger.error(f"Given directory '{config.source}' does not exist!")
        return

    if not config.source.is_dir():
        logger.error(f"Given path '{config.source}' is not a directory!")
        return

    logger.info(f"Start single backup of directory '{config.source}'")

    backup = Backupr(config)
    has_changed = backup.has_changed()

    if has_changed or config.force:
        logger.info(f"Directory '{config.source}' has been modified.'")
        logger.debug(f"'force_rewrite'='{config.force}'")

        backup.backup_now()
        backup.export_store()

        logger.info(f"Finished single backup")
    else:
        logger.info("Nothing has changed.")
