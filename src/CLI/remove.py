"""
Filename: remove.py
Created on 26.05.2020
Creator: Myrtium
"""
# External modules
import logging
from pathlib import Path

# Project related modules
import constants as const
import CLI.parameter as param
from config import Config
from _logging import init_logging
from util import gen_argument, load_json, write_json

logger = logging.getLogger().getChild(__name__)

PARAMETER = [
    param.CONFIG_NAME,
    param.CONFIG_PATH
]


def generate_subparser_remove(subparser):
    parser = subparser.add_parser("remove", help="Remove a directory from backup process")

    for arg in PARAMETER:
        gen_argument(parser, arg)
    parser.set_defaults(func=remove_backup)


def remove_backup(args):
    name = args.name
    config_path = Path(args.path or const.BACKUPR_DEFAULT_CONFIG_PATH)

    config = Config.from_dict(load_json(config_path))

    to_remove = config.backup_dict.get(name)

    if to_remove is None:
        print(f"Configuration '{name}' not found!")
    else:
        del config.backup_dict[name]
        print(f"Configuration '{name}' removed.")

    write_json(config.to_dict(serializable=True), config_path)
