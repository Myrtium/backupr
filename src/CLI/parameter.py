"""
Filename: parameter.py
Created on 26.05.2020
Creator: Myrtium
"""
# External modules
import logging

# Project related modules
import constants as const

logger = logging.getLogger().getChild(__name__)

WAIT = {
    "NAME": "--wait",
    "SHORT": "-w",
    "HELP": "Time to elapse until repeat",
    "TYPE": str,
    "DEFAULT": const.BACKUPR_DEFAULT_WAIT,
}

LOG = {
    "NAME": "--log",
    "SHORT": "-l",
    "HELP": "Set logfile path",
    "TYPE": str,
    "DEFAULT": const.BACKUPR_DEFAULT_LOGFILE_PATH,
}

LOGLEVEL = {
    "NAME": "--loglevel",
    "HELP": "Set loglevel",
    "TYPE": str,
    "CHOICES": const.BACKUPR_LOGLEVELS.keys(),
    "DEFAULT": "INFO",
}

SRC_PATH = {
    "NAME": "src",
    "HELP": "Source directory to backup",
    "TYPE": str,
}

SRC_PATH_OPTIONAL = {
    "NAME": "--source",
    "SHORT": "-s",
    "HELP": "Source directory to backup",
    "TYPE": str,
}

DEST_FILE = {
    "NAME": "dest",
    "HELP": "Backup file path. When no filename given, source-filename is being used.",
    "TYPE": str,
}

DEST_FILE_OPTIONAL = {
    "NAME": "--dest",
    "SHORT": "-d",
    "HELP": "Backup file path. When no filename given, source-filename is being used.",
    "TYPE": str,
}

FORCE_BACKUP = {
    "NAME": "--force",
    "SHORT": "-f",
    "HELP": "Force backup",
    "DEFAULT": False,
}

FORCE_CONFIG_INIT = {
    "NAME": "--force",
    "SHORT": "-f",
    "HELP": "Force initialization of config file. Existing file will be overridden!",
    "ACTION": "store_true",
}

CONFIG_PATH = {
    "NAME": "--path",
    "SHORT": "-p",
    "HELP": "Path to config file",
    "TYPE": str,
    "DEFAULT": const.BACKUPR_DEFAULT_CONFIG_PATH,
}

NEW_CONFIG_PATH = {
    "NAME": "--path",
    "SHORT": "-p",
    "HELP": "Path to new config file",
    "TYPE": str,
    "DEFAULT": const.BACKUPR_DEFAULT_CONFIG_PATH,
}

RECURSIVE = {
    "NAME": "--recursive",
    "SHORT": "-r",
    "HELP": "Backup files and directories recursively",
    "DEFAULT": True,
}

IGNORE_HIDDEN = {
    "NAME": "--ignore-hidden",
    "SHORT": "-i",
    "HELP": "Ignore hidden files (starting with a .)",
    "DEFAULT": False,
}

IGNORE_FILE = {
    "NAME": "--ignore-file",
    "HELP": "Ignore given files (relative path)",
    "TYPE": str,
    "NARGS": "*",
}

OVERRIDE_IGNORE_FILE = {
    "NAME": "--override-ignore-file",
    "SHORT": "-o",
    "HELP": "Override current ignoring list",
    "ACTION": "store_true",
}

COMPRESSION = {
    "NAME": "--compression",
    "SHORT": "-c",
    "CHOICES": const.BACKUPR_COMPRESSION_METHODS.keys(),
    "TYPE": str,
    "HELP": "Compression method",
    "DEFAULT": "deflated",
}

CONFIG_NAME = {
    "NAME": "name",
    "TYPE": str,
    "HELP": "Configuration name"
}

CONFIG_NAME_OPTIONAL = {
    "NAME": "--name",
    "SHORT": "-n",
    "TYPE": str,
    "HELP": "Name of configuration",
    "DEFAULT": f"Random generated {const.BACKUPR_DEFAULT_CONFIG_NAME_LENGTH} letter long string"
}

DETAIL = {
    "NAME": "--detail",
    "SHORT": "-d",
    "HELP": "Show details",
    "ACTION": "store_true",
}

BACKGROUND = {
    "NAME": "--background",
    "HELP": "Start Service in background",
    "ACTION": "store_true",
}

ROTATE = {
    "NAME": "--rotate",
    "SHORT": "-t",
    "HELP": "Number of rotations of backup.",
    "TYPE": int,
    "DEFAULT": 0,
}

