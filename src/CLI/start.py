"""
Filename: start.py
Created on 26.05.2020
Creator: Myrtium
"""
# External modules
import logging
import time
from pathlib import Path

# Project related modules
import constants as const
import CLI.parameter as param
from service import Service
from _logging import init_logging
from util import load_json, gen_argument, parse_time_to_seconds, str2bool
from config import Config

logger = logging.getLogger().getChild(__name__)

PARAMETER = [
    param.CONFIG_PATH,
    param.WAIT,
    param.LOG,
    param.LOGLEVEL,
    param.BACKGROUND,
]


def generate_subparser_service(subparser):
    parser = subparser.add_parser("service", help="Start as a service")

    for arg in PARAMETER:
        gen_argument(parser, arg)
    parser.set_defaults(func=start_service)


def start_service(args):
    config_path = Path(args.path or const.BACKUPR_DEFAULT_CONFIG_PATH)

    if config_path.is_file() and config_path.exists():
        config = Config.from_dict(load_json(config_path))

        logfile_path = args.log or config.logfile or const.BACKUPR_DEFAULT_LOGFILE_PATH
        loglevel = args.loglevel or config.loglevel or const.BACKUPR_DEFAULT_LOG_LEVEL
        init_logging(logfile_path, loglevel)

        wait = parse_time_to_seconds(args.wait or config.wait or const.BACKUPR_DEFAULT_WAIT)
        assert wait != (float or int)
        in_background = str2bool(args.background, False)

        if in_background:
            from GUI import start_gui
            start_gui(config)
        else:
            service = Service(config)
            # Loop until interrupt
            while True:
                service.run()
                time.sleep(wait)
    else:
        logfile_path = args.log or const.BACKUPR_DEFAULT_LOGFILE_PATH
        loglevel = args.loglevel or const.BACKUPR_DEFAULT_LOG_LEVEL

        init_logging(logfile_path, loglevel)
        logger.error(f"File {config_path} not found! Please use 'backupr init' to create a config file.")