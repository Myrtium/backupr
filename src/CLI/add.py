"""
Filename: add.py
Created on 26.05.2020
Creator: Myrtium
"""
# External modules
from pathlib import Path

# Project related modules
import constants as const
import CLI.parameter as param
from util import gen_argument, str2bool, load_json, write_json, random_generator
from config import Config
from exceptions import ConfigError
from backup import Backup

PARAMETER = [
    param.SRC_PATH,
    param.DEST_FILE,
    param.CONFIG_PATH,
    param.FORCE_BACKUP,
    param.RECURSIVE,
    param.IGNORE_HIDDEN,
    param.IGNORE_FILE,
    param.COMPRESSION,
    param.CONFIG_NAME_OPTIONAL,
    param.ROTATE,
]


def generate_subparser_add(subparser):
    parser = subparser.add_parser("add", help="Add a directory to monitor and backup")

    for arg in PARAMETER:
        gen_argument(parser, arg)
    parser.set_defaults(func=add_backup)


def add_backup(args):
    source = Path(args.src)
    destination = Path(args.dest)
    config_path = Path(args.path or const.BACKUPR_DEFAULT_CONFIG_PATH)
    recursive = str2bool(args.recursive, const.BACKUPR_DEFAULT_RECURSIVE_MODE)
    ignore_hidden = str2bool(args.ignore_hidden, const.BACKUPR_DEFAULT_IGNORE_HIDDEN_MODE)
    force = str2bool(args.force, const.BACKUPR_DEFAULT_FORCE_MODE)
    compression = args.compression or const.BACKUPR_DEFAULT_COMPRESSION
    ignore_file = args.ignore_file
    name = args.name
    rotations = args.rotate or const.BACKUPR_DEFAULT_ROTATIONS

    config = Config.from_dict(load_json(config_path))

    used_names = []
    used_dirs = []
    for curr_name, curr_config in config.backup_dict.items():
        used_names.append(curr_name)
        used_dirs.append(curr_config.source)

    if source in used_dirs:
        raise ConfigError(f"Directory {source} is already configured to be backuped!")

    if name is None:
        name = random_generator(const.BACKUPR_DEFAULT_CONFIG_NAME_LENGTH)
        while name in used_names:
            name = random_generator(const.BACKUPR_DEFAULT_CONFIG_NAME_LENGTH)
    elif name in used_names:
        raise ConfigError(f"Name '{name}' is already used")

    if rotations < 0:
        raise ConfigError(f"Cannot rotate '{rotations}' times!")

    new_config = Backup(
        source=source,
        destination=destination,
        force=force,
        recursive=recursive,
        ignore_hidden=ignore_hidden,
        compression=compression,
        rotations=rotations,
        ignore_file=ignore_file
    )

    config.backup_dict[name] = new_config
    write_json(config.to_dict(serializable=True), config_path)
