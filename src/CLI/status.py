"""
Filename: status.py
Created on 26.05.2020
Creator: Myrtium
"""
# External modules
from pathlib import Path

# Project related modules
import constants as const
import CLI.parameter as param
from util import gen_argument, str2bool, load_json, write_json, merge
from config import Config
from exceptions import ConfigError
from backup import Backup

PARAMETER = [
    param.CONFIG_PATH,
    param.DETAIL
]


def generate_subparser_status(subparser):
    parser = subparser.add_parser("status", help="Show status of configuration")

    for arg in PARAMETER:
        gen_argument(parser, arg)
    parser.set_defaults(func=show_status)


def show_status(args):
    config_path = Path(args.path or const.BACKUPR_DEFAULT_CONFIG_PATH)
    detail = args.detail

    config = Config.from_dict(load_json(config_path))

    print(config.to_str(detail))
