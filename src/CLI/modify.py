"""
Filename: modify.py
Created on 26.05.2020
Creator: Myrtium
"""
# External modules
from pathlib import Path

# Project related modules
import constants as const
import CLI.parameter as param
from util import gen_argument, str2bool, load_json, write_json, merge
from config import Config
from exceptions import ConfigError
from backup import Backup

PARAMETER = [
    param.CONFIG_NAME,
    param.SRC_PATH_OPTIONAL,
    param.DEST_FILE_OPTIONAL,
    param.CONFIG_PATH,
    param.FORCE_BACKUP,
    param.RECURSIVE,
    param.IGNORE_HIDDEN,
    param.IGNORE_FILE,
    param.OVERRIDE_IGNORE_FILE,
    param.COMPRESSION,
    param.ROTATE,
]


def generate_subparser_modify(subparser):
    parser = subparser.add_parser("modify", help="Modify a configuration")

    for arg in PARAMETER:
        gen_argument(parser, arg)
    parser.set_defaults(func=modify_backup)


def modify_backup(args):
    name = args.name
    source = args.source
    destination = args.dest
    config_path = Path(args.path or const.BACKUPR_DEFAULT_CONFIG_PATH)
    recursive = str2bool(args.recursive)
    ignore_hidden = str2bool(args.ignore_hidden)
    force = str2bool(args.force)
    compression = args.compression
    ignore_file = args.ignore_file
    override_ignore = str2bool(args.override_ignore_file, False)
    rotations = args.rotate

    config = Config.from_dict(load_json(config_path))

    used_names = []
    used_dirs = []
    for curr_name, curr_config in config.backup_dict.items():
        used_names.append(curr_name)
        used_dirs.append(curr_config.source)

    if source in used_dirs:
        raise ConfigError(f"Directory {source} is already configured to be backuped!")

    if name in used_names:
        old_config = config.backup_dict[name]

        merged_ignore_files = ignore_file if override_ignore else set(ignore_file + old_config.ignore_file)

        new_config = Backup(
            source=Path(merge(source, old_config.source)),
            destination=Path(merge(destination, old_config.destination)),
            force=merge(force, old_config.force),
            recursive=merge(recursive, old_config.recursive),
            ignore_hidden=merge(ignore_hidden, old_config.ignore_hidden),
            compression=merge(compression, old_config.compression),
            ignore_file=merge(merged_ignore_files, old_config.ignore_file),
            rotations=merge(rotations, old_config.rotations),
        )

        config.backup_dict[name] = new_config
        write_json(config.to_dict(serializable=True), config_path)
