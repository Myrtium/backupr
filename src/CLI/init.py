"""
Filename: init.py
Created on 26.05.2020
Creator: Myrtium
"""
# External modules
import logging
from pathlib import Path

# Project related modules
import constants as const
import CLI.parameter as param
from util import gen_argument, str2bool, write_json
from config import Config
from _logging import init_logging


logger = logging.getLogger().getChild(__name__)

PARAMETER = [
    param.NEW_CONFIG_PATH,
    param.LOGLEVEL,
    param.LOG,
    param.WAIT,
    param.FORCE_CONFIG_INIT,
]


def generate_subparser_init(subparser):
    parser = subparser.add_parser("init", help="Initialize config file for service")

    for arg in PARAMETER:
        gen_argument(parser, arg)
    parser.set_defaults(func=init_config)


def init_config(args):
    config_path = Path(args.path or const.BACKUPR_DEFAULT_CONFIG_PATH)
    logfile_path = args.log or const.BACKUPR_DEFAULT_LOGFILE_PATH
    loglevel = args.loglevel or const.BACKUPR_DEFAULT_LOG_LEVEL
    wait = args.wait or const.BACKUPR_DEFAULT_WAIT
    force = str2bool(args.force, False) # Force to overwrite existing config file

    init_logging(logfile_path, loglevel)

    if not force and config_path.exists() and config_path.is_file():
        logger.warning(f"File '{config_path}' already exist. Use -f or --force to overwrite.")
    else:
        config = Config(logfile=logfile_path, loglevel=loglevel, wait=wait)
        write_json(config.to_dict(serializable=True), config_path)
        logger.info(f"Config file created at '{config_path}'.")
