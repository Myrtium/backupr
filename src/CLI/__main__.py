"""
Filename: __main__.py
Created on 26.05.2020
Creator: Myrtium
"""
# External modules
import logging
import argparse
import sys

# Project related modules
from CLI.add import generate_subparser_add
from CLI.init import generate_subparser_init
from CLI.start import generate_subparser_service
from CLI.single import generate_subparser_single
from CLI.remove import generate_subparser_remove
from CLI.modify import generate_subparser_modify
from CLI.status import generate_subparser_status

logger = logging.getLogger().getChild(__name__)

# Check if this code file is being executed in an IPython console
try:
    # noinspection PyUnresolvedReferences
    __IPYTHON__
except NameError:
    in_ipython = False
else:
    in_ipython = True

# If code file is being executed in an IPython console ...
if in_ipython:
    # Expand IPython exception traceback method with logging of all exceptions
    from IPython.core.interactiveshell import InteractiveShell

    base_showtraceback = InteractiveShell.showtraceback

    # noinspection PyMissingOrEmptyDocstring
    def showtraceback(self, exc_tuple=None, filename=None, tb_offset=None, exception_only=False):
        logger.critical("Code execution failed", exc_info=True)
        base_showtraceback(self, exc_tuple, filename, tb_offset, exception_only)


    InteractiveShell.showtraceback = showtraceback


default_excepthook = sys.__excepthook__


def logger_excepthook(exc_type, exc_value, exc_traceback):
    logger.critical("Code execution failed", exc_info=(exc_type, exc_value, exc_traceback))


sys.excepthook = logger_excepthook


def show_help(args):
    parser.print_help()


if __name__ == "__main__":
    parser = argparse.ArgumentParser(prog="Backupr")
    parser.set_defaults(func=show_help)

    subparsers = parser.add_subparsers(help="Select between")
    generate_subparser_single(subparsers)
    generate_subparser_init(subparsers)
    generate_subparser_add(subparsers)
    generate_subparser_service(subparsers)
    generate_subparser_remove(subparsers)
    generate_subparser_modify(subparsers)
    generate_subparser_status(subparsers)

    args = parser.parse_args()
    args.func(args)