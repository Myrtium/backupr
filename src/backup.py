"""
Filename: backup.py
Created on 24.05.2020
Creator: Myrtium
"""

import constants as const
from dataclasses import dataclass, field
from util import random_generator
from pathlib import Path
from typing import List


@dataclass
class Backup:
    source: Path
    destination: Path
    force: bool
    recursive: bool
    ignore_hidden: bool
    compression: str
    rotations: int
    ignore_file: List[Path] = field(default_factory=list)

    def __post_init__(self):
        if not isinstance(self.source, Path):
            self.source = Path(self.source)

        if not isinstance(self.destination, Path):
            self.destination = Path(self.destination)

        if self.force is None:
            self.force = const.BACKUPR_DEFAULT_FORCE_MODE

        if self.recursive is None:
            self.recursive = const.BACKUPR_DEFAULT_RECURSIVE_MODE

        if self.ignore_hidden is None:
            self.ignore_hidden = const.BACKUPR_DEFAULT_IGNORE_HIDDEN_MODE

        if self.compression is None:
            self.compression = const.BACKUPR_DEFAULT_COMPRESSION

        self.compression_int = const.BACKUPR_COMPRESSION_METHODS.get(self.compression)

        if self.rotations is None:
            self.rotations = const.BACKUPR_DEFAULT_ROTATIONS

        if self.ignore_file is None:
            self.ignore_file = []

    @staticmethod
    def from_dict(input: dict) -> "Backup":
        source = input.get("source")
        destination = input.get("destination")
        force = input.get("force")
        recursive = input.get("recursive")
        ignore_hidden = input.get("ignore_hidden")
        compression = input.get("compression")
        ignore_file = input.get("ignore_file")
        rotations = input.get("rotations")

        back = Backup(
            source=source,
            destination=destination,
            force=force,
            recursive=recursive,
            ignore_hidden=ignore_hidden,
            compression=compression,
            ignore_file=ignore_file,
            rotations=rotations,
        )

        return back

    def to_dict(self, serializable=False):
        if self.ignore_file is not None:
            ignore_files = [str(file) for file in self.ignore_file] if serializable else self.ignore_file
        else:
            ignore_files = []

        output = {
            "source": str(self.source) if serializable else self.source,
            "destination": str(self.destination) if serializable else self.destination,
            "force": self.force,
            "recursive": self.recursive,
            "ignore_hidden": self.ignore_hidden,
            "compression": self.compression,
            "rotations": self.rotations,
            "ignore_file": ignore_files,
        }

        return output

    def to_str(self, detailed=False):
        output = f"""            source: {str(self.source)},
            destination: {str(self.destination)},
            force: {self.force},
            recursive: {self.recursive},
            ignore_hidden: {self.ignore_hidden},
            compression: {self.compression},
            rotations: {self.rotations},
            ignore_file: """
        if detailed:
            output += "["
            for file in self.ignore_file:
                output += "\n               " + str(file) + ","
            output += "\n            ]"
        else:
            output += f"[+{len(self.ignore_file)}]"

        return output
