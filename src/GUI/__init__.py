"""
Filename: __init__.py
Created on 25.06.2020
Creator: Myrtium
"""
# External modules
import logging
import sys
from PyQt5.QtWidgets import QApplication

# Project related modules
import resource_handler
import constants as const
from GUI.tray_icon import TrayIcon
from config import Config
from util import load_json, get_root_directory

logger = logging.getLogger().getChild(__name__)


def init_resource_handler():
    rh = resource_handler.ResourceHandler(get_root_directory(), const.BACKUPR_RESOURCE_FILE_PATH)
    resource_handler.set_resource_handler(rh)

    return rh


def start_gui(config):
    app = QApplication([])
    init_resource_handler()
    TrayIcon(config)
    app.exec_()
