"""
Filename: tray_icon.py
Created on 25.06.2020
Creator: Myrtium
"""
# External modules
import logging
from PyQt5.QtCore import QCoreApplication, QTimer
from PyQt5.QtWidgets import QMainWindow, QSystemTrayIcon, QMenu, QAction
from PyQt5.QtGui import QIcon

# Project related modules
import resource_handler as rh
from util import parse_time_to_seconds
from service import Service
from worker import Worker

logger = logging.getLogger().getChild(__name__)


class TrayIcon(QMainWindow):
    def __init__(self, config, parent=None):
        super(TrayIcon, self).__init__(parent)
        self.config = config

        self.system_tray = None
        self.worker = None
        self.timer = None

        self.service = Service(config)
        self.initialize()

    def initialize(self):
        self.create_system_tray()
        self.create_worker()
        self.create_timer()

        self.system_tray.show()
        self.system_tray.showMessage("Backupr started!", "Running in background...")
        self.worker.start()

    def create_system_tray(self):
        self.system_tray = QSystemTrayIcon(self)
        tray_icon_ico = QIcon(str(rh.get_resource_handler().get("backupr_icon")))

        tray_icon_menu = QMenu(self)
        action_quit = QAction("Quit", self)
        action_quit.triggered.connect(QCoreApplication.exit)
        tray_icon_menu.addAction(action_quit)
        self.system_tray.setIcon(tray_icon_ico)
        self.system_tray.setContextMenu(tray_icon_menu)

    def create_timer(self):
        self.timer = QTimer(self)
        self.timer.setInterval(parse_time_to_seconds(self.config.wait)*1000)
        self.timer.timeout.connect(self.worker.start)

    def start_timer(self):
        self.timer.start()

    def create_worker(self):
        self.worker = Worker(self, self._run)
        self.worker.error.connect(lambda x: logger.error(x))
        self.worker.done.connect(self.start_timer)

    def _run(self):
        self.timer.stop()
        self.service.run()



