"""
Filename: exceptions.py
Created on 19.09.2019
Creator: Myrtium
"""
# External modules
import logging

# Project related modules

logger = logging.getLogger().getChild(__name__)


class UnknownUnitError(Exception):
    pass


class ParseError(Exception):
    pass


class ConfigError(Exception):
    pass


class DirectoryDoesNotExistError(Exception):
    pass
