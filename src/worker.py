"""
Filename: woker.py
Created on 26.06.2020
Creator: Myrtium
"""
# External modules
import logging
from PyQt5 import QtCore

# Project related modules

logger = logging.getLogger().getChild(__name__)
Qt = QtCore.Qt


class Worker(QtCore.QThread):

    success = QtCore.pyqtSignal(object)
    error = QtCore.pyqtSignal(Exception)
    done = QtCore.pyqtSignal()


    def __init__(self, parent, function, *args, **kwargs):
        super(Worker, self).__init__(parent)

        self.function = function
        self.function_args = args
        self.function_kwargs = kwargs

    def run(self):
        try:
            value = self.function(*self.function_args, **self.function_kwargs)
            self.success.emit(value)
        except Exception as e:
            self.error.emit(e)
        finally:
            self.done.emit()
