"""
Filename: util.py
Created on 27.08.2019
Creator: Myrtium
"""
# External modules
import logging
import json
import zipfile
import random
import string
import sys
import os
from pathlib import Path, WindowsPath, PosixPath, PureWindowsPath, PurePosixPath, PurePath
# Project related modules
from exceptions import UnknownUnitError, ParseError

logger = logging.getLogger().getChild(__name__)


def is_hidden(path):
    if type(path) == str:
        path = Path(path)

    if type(path) == Path or type(path) == WindowsPath or type(path) == PosixPath or type(path) == PureWindowsPath or type(path) == PurePosixPath or type(path) == PurePath:
        return path.name.startswith(".")


def load_json(path):
    path = Path(path)

    with open(path, "r") as file:
        loaded = json.loads((file.read()))

    return loaded


def write_json(load, dest):
    path = Path(dest)

    dump = json.dumps(load)
    with open(path, "w") as file:
        file.write(dump)


def zip_it(dest, root_path, files, compression):
    with zipfile.ZipFile(dest, "w", compression=compression) as zipper:
        for file in files:
            zipper.write(root_path/file, arcname=file)


def parse_time_to_seconds(time):
    possible_units = ["s", "m", "h", "d"]

    if time is None:
        return -1

    unit = time[-1]
    time = int(time[:-1])

    if unit.lower() not in possible_units:
        raise UnknownUnitError(f"Given unit '{unit}' unknown. Must be one of '{possible_units}'")

    if unit == "s":
        return time

    elif unit == "m":
        return time * 60

    elif unit == "h":
        return time * 3600

    elif unit == "d":
        return time * 86_400


def gen_argument(parser, parameter):
    name = parameter["NAME"]
    short = parameter.get("SHORT")
    help = parameter.get("HELP")
    type = parameter.get("TYPE")
    default = parameter.get("DEFAULT")
    action = parameter.get("ACTION")
    nargs = parameter.get("NARGS")
    choices = parameter.get("CHOICES")

    if default is not None:
        help += f". Default: '{default}'"

    # Default values are not set on purpose. They are handled on premise (see __init__).
    if short is not None:
        if action is not None:
            parser.add_argument(short, name, help=help, action=action)
        elif nargs is not None:
            parser.add_argument(short, name, help=help, nargs=nargs, type=type)
        elif choices is not None:
            parser.add_argument(short, name, help=help, choices=choices, type=type)
        else:
            parser.add_argument(short, name, help=help, type=type)
    else:
        if action is not None:
            parser.add_argument(name, help=help, action=action)
        elif nargs is not None:
            parser.add_argument(name, help=help, nargs=nargs, type=type)
        elif choices is not None:
            parser.add_argument(name, help=help, choices=choices, type=type)
        else:
            parser.add_argument(name, help=help, type=type)


# https://stackoverflow.com/questions/15008758/parsing-boolean-values-with-argparse
def str2bool(v: str, default=None):
    if v is None:
        return default
    if isinstance(v, bool):
        return v
    if v.lower() in ('yes', 'true', 't', 'y', '1', 'on'):
        return True
    elif v.lower() in ('no', 'false', 'f', 'n', '0', 'off'):
        return False
    else:
        raise ParseError('Boolean value expected.')


# https://stackoverflow.com/questions/2257441/random-string-generation-with-upper-case-letters-and-digits
def random_generator(num):
    return "".join(random.choices(string.ascii_lowercase + string.digits, k=num))


# Left is dominant
def merge(left, right):
    if left is not None:
        return left
    else:
        return right


def get_root_directory():
    is_frozen = getattr(sys, 'frozen', False)
    frozen_temp_path = getattr(sys, '_MEIPASS', '')

    if is_frozen:
        root_directory = frozen_temp_path
    else:
        directory = os.path.dirname(__file__)
        root_directory = os.path.abspath(os.path.join(directory, ".."))

    return root_directory