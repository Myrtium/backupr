"""
File: store.py
Created on 27.08.2019
Creator: Myrtium
"""
# External modules
import logging
from pathlib import Path

# Project related modules
from util import is_hidden
from exceptions import DirectoryDoesNotExistError

logger = logging.getLogger().getChild(__name__)


class Store:

    def __init__(self, files: dict = None, meta: dict = None):
        if files is None:
            self.files = {}
        else:
            self.files = {Path(x) if not isinstance(x, Path) else x: files[x] for x in files}

        if meta is None:
            self.meta = {}
        else:
            self.meta = meta

    def equal_to(self, other: "Store"):

        if other is None:
            return False

        if self.files != other.files:
            return False

        if self.meta != other.meta:
            return False

        return True

    @staticmethod
    def from_dict(load: dict):
        if load is None:
            files = {}
            meta = {}
        else:
            files = load.get("files") or {}
            meta = load.get("meta") or {}

        files = {Path(x): files[x] for x in files}
        root = meta.get("root")
        if root:
            meta["root"] = Path(root)

        return Store(files, meta)

    def to_dict(self, serializable=False):
        if serializable:
            files = {str(file): self.files[file] for file in self.files}
        else:
            files = self.files

        return {"meta": self.meta, "files": files}

    @staticmethod
    def index_files(root: Path, recursive: bool, ignore_hidden: bool, ignore_file: list = None):

        if ignore_file is None:
            ignore_file = []

        assert ignore_file != list

        # Convert all entries to Paths
        ignore_file = [Path(entry) for entry in ignore_file]

        root_path = Path(root)

        if not root_path.exists():
            raise DirectoryDoesNotExistError(f"Given directory '{root_path}' does not exist!")

        if not root_path.is_dir():
            raise NotADirectoryError(f"Given path '{root}' is not a directory!")

        directories = [root_path]
        files = {}

        dir_counter = 0
        file_counter = 0

        while len(directories) > 0:
            directory = directories.pop()

            for curr_path in directory.iterdir():
                is_valid = True
                new_path = Path(curr_path)  # Make Path object for easier process

                if new_path.relative_to(root_path) in ignore_file:
                    is_valid = False

                if is_valid and is_hidden(new_path) and ignore_hidden is True:
                    is_valid = False

                if is_valid and new_path.is_dir():
                    dir_counter += 1  # Increment directory counter

                    # If directories within given directory are supposed to be indexed as well.
                    if recursive is not False:
                        directories.append(new_path)
                elif is_valid and new_path.is_file():
                    file_counter += 1  # Increment file counter

                if is_valid is True:
                    files[curr_path.relative_to(root_path)] = curr_path.stat().st_mtime

        meta = {
            "root": root_path,
            "directories": dir_counter,
            "files": file_counter
        }

        return Store(files, meta)
