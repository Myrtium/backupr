# Backupr

Backupr is small tool to occasionally compress and backup a specified directories. It is written in Python 3.7 and can be used on Windows, Linux and macOS.

[Link to Documentation](https://gitlab.com/Myrtium/backupr/-/wikis/home)

## Motivation
I am a computer scientist student and at university we use a lot of different Software that sometimes creates tons of files in a project. Since I am using the all mighty cloud to syncronize all my stuff, these project folders can be really annoying. With Backupr I can keep the projects local on my computer and still have a (most time) up to date version in the cloud.
When developing the first version, something came to my mind: Why don't use it for any kind of directory?
With this version I generalized the behavior so you can use it for (probably almost) every kind of directory.