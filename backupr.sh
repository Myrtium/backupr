#/bin/bash

if ! [[ "$(python3 -V)" =~ "Python 3" ]]; then
	echo Error: python3 could not be found.
	echo Please install python3
	exit 1
fi

#SOURCE="$( cd "$( dirname "${BASH_SOURCE[0]}" )" > /dev/null 2>&1 $$ pwd )"
#echo "(dirname $0)"

dir=$(dirname "$(readlink -f "$0")")

export PYTHONPATH="$dir/src"
echo $PYTHONPATH
python3 -m CLI "$@"
